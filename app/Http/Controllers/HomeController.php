<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function table()
    {
        return view('simple');
    }

    public function data_tables()
    {
        return view('data');
    }
}
